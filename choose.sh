#!/bin/sh
# 
# Choose a "minimum common restaurant" given a list of participants


# vars
PROGNAME=${0##*/}


# functions
usage() {
    cat <<EOF
Choose a "minimum common restaurant" given a list of participants
Usage: ${PROGNAME} [-h] [-e <restaurant>] [<not-present-1> <not-present-2> <...>]

Options:
     -h : display this help and exit
     -e : exclude restaurant
EOF
}


# checks
EXCLUDE_RESTAURANT=''
while getopts "he:" OPTION; do
    case $OPTION in
        h)
            usage
            exit 0
            ;;
        e)
	    EXCLUDE_RESTAURANT="${OPTARG}"
	    ;;
        *)
            usage
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

FILE_RESTAURANTS='restaurants.txt'
FILE_PARTICIPANTS='participants.txt'
for I in ${FILE_RESTAURANTS} ${FILE_PARTICIPANTS}; do
    if [ ! -s "${I}" ]; then
	echo "E: needed file '${I}' does not exist or is empty."
	exit 1
    fi
done


# main
MISSING_PARTICIPANTS="$@"
TMPFILE_PARTICIPANTS="$(mktemp)"
cp ${FILE_PARTICIPANTS} ${TMPFILE_PARTICIPANTS}
for I in ${MISSING_PARTICIPANTS}; do
    sed -i -e "/${I}/d" ${TMPFILE_PARTICIPANTS}
done
CONFIRMED_PARTICIPANTS="$(grep -v -e '^#' ${TMPFILE_PARTICIPANTS})"
rm -f ${TMPFILE_PARTICIPANTS}

TMPFILE_RESTAURANTS="$(mktemp)"
### ATTENTION, avoid sorting errors reported by `comm` later on!
sed -e "/^#/d" ${FILE_RESTAURANTS} >${TMPFILE_RESTAURANTS}
[ -n "${EXCLUDE_RESTAURANT}" ] && \
 sed -i -e "/${EXCLUDE_RESTAURANT}/d" ${TMPFILE_RESTAURANTS}
for I in ${CONFIRMED_PARTICIPANTS}; do
    sed -i -e "/${I}/d" ${TMPFILE_RESTAURANTS}
done
grep -v -e "^#" ${FILE_RESTAURANTS} | \
 comm -1 - ${TMPFILE_RESTAURANTS} | \
 grep -v -e '#' | \
 cut -d ',' -f 1 | \
 sed -e 's/^[[:space:]]*//' | \
 sort -R | \
 head -n 1
rm -f ${TMPFILE_RESTAURANTS}
